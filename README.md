<!--
SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen

SPDX-License-Identifier: CC0-1.0
-->

# TextGrid FCS Endpoint

A [Federated Content Search ](https://www.clarin.eu/content/federated-content-search-clarin-fcs-technical-details)
endpoint for the [TextGrid Repository](https://textgridrep.org/).

## Run / Develop

With venv

```bash
python -m venv venv
. venv/bin/activate
pip install -e .[dev]

python -m tg-fcs-endpoint.service
```

or docker compose:

```bash
docker compose up --build
```

and search for alice:
<http://localhost:5000/TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c/fcs?query=alice&maximumRecords=10>


or check the endpoint description:
<http://localhost:5000/TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c/fcs?operation=explain&version=1.2&x-fcs-endpoint-description=true>

## FCS aggregator / text+ search widget

Docker compose also starts the [FCS-SRU-Aggregator](https://github.com/clarin-eric/fcs-sru-aggregator) and shows the [Text+ Vuetify Advanced Search](https://git.saw-leipzig.de/text-plus/FCS/textplus-fcs-vuetify) widget.

You can access this at

* http://localhost:8000 - the search widget
* http://localhost:8000/aggregator - the aggregator
* http://localhost:8001 - the traefik interface ;-)

if you want to add own projects edit the
[./compose/aggregator/aggregator_devel.yml](./compose/aggregator/aggregator_devel.yml)

and run:

```bash
docker compose up --build
```

## Update Dependencies

This repo ships a [requirements.txt](requirements.txt) with fixed version numbers and hashes of python dependencies, to ensure reproducible builds. Docker images are build from this requirements.txt.
To update dependencies from pyproject.toml run `./update-requirements.sh` in venv.

## References

* https://github.com/Querela/fcs-korp-endpoint-python/
* https://github.com/cceh/kosh/commit/1785582775dde1839e4b2f106625e0bd79d86141
* https://docs.google.com/presentation/d/16X5b0eUYjSupwuhdJZevgLNvzWQ6U6-YdgGOtwKwqDI/edit#slide=id.g296a7d5e169_0_29

## Badges

[![REUSE status](https://api.reuse.software/badge/gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint)](https://api.reuse.software/info/gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint)
[![Dependency Track Status](https://deps.sub.uni-goettingen.de/api/v1/badge/vulns/project/tg-fcs-endpoint/develop)](https://deps.sub.uni-goettingen.de/projects/f02ca31c-69b0-417a-abd8-98483babc448)
[![Dependency Track Container Status](https://deps.sub.uni-goettingen.de/api/v1/badge/vulns/project/tg-fcs-endpoint-container/develop)](https://deps.sub.uni-goettingen.de/projects/dead40c2-6919-4aeb-8c5e-de80238bdf73)
