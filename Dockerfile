# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

# the builder
FROM python:3.11 as builder
RUN mkdir /app
WORKDIR /app/
ADD . /app/
RUN pip install -r requirements.txt

# the app (slim)
FROM python:3.11-slim-bookworm

COPY --from=builder /usr/local /usr/local
COPY --from=builder /app /app

WORKDIR /app/
CMD ["gunicorn", "--bind", "0.0.0.0:80", "--pythonpath", "src", "tg-fcs-endpoint.service:app"]
