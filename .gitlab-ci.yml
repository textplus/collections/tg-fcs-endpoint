# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

include:
  - project: 'dariah-de/gitlab-templates'
    ref: 'main'
    file:
      - '/templates/.deps.gitlab-ci.yml'

variables:
  CONTAINER_IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_TITLE
  PIP_REQUIREMENTS_FILE: /app/requirements.txt

.kaniko-setup_template: &kaniko-setup
  - mkdir -p /kaniko/.docker
  - echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(echo -n ${CI_REGISTRY_USER}:${CI_REGISTRY_PASSWORD} | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json

.crane-setup_template: &crane-setup
  - crane auth login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

workflow:
  rules:
    - if: $CI_COMMIT_REF_NAME == "main" && $CI_COMMIT_TITLE =~ /^\d+.\d+.\d+$/
      when: never
    - if: $CI_COMMIT_TAG =~ /^v\d+.\d+.\d+$/
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

stages:
  - build
  - test
  - deploy

##
# run always
##
build:
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  stage: build
  before_script:
    - *kaniko-setup
  script:
    # debug the workflow rule, remove this line later
    - echo CI_COMMIT_REF_NAME is $CI_COMMIT_REF_NAME / CI_COMMIT_TITLE is $CI_COMMIT_TITLE / CI_COMMIT_BRANCH is $CI_COMMIT_BRANCH / CI_COMMIT_TAG is $CI_COMMIT_TAG
    - /kaniko/executor
      --context $CI_PROJECT_DIR
      --dockerfile $CI_PROJECT_DIR/Dockerfile
      --destination $CONTAINER_IMAGE

dependency-scanning:
  stage: test
  image: python:3.11-slim-bookworm
  needs: []
  script:
    - pip install cyclonedx-bom
    - cyclonedx-py requirements requirements.txt --output-format json -o bom.json
  artifacts:
    paths:
      - 'bom.json'

container-scanning:
  stage: test
  image:
    name: anchore/syft:debug
    entrypoint: [""]
  script:
    - /syft $CONTAINER_IMAGE -o cyclonedx-json=sbom.container.json
  artifacts:
    paths:
      - 'sbom.container.json'

##
# runs on every branch except tags
##

tag-dev-image:
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  stage: deploy
  before_script:
    - *crane-setup
  script:
    - crane tag $CONTAINER_IMAGE latest
  rules:
    - if: $CI_COMMIT_TAG
      when: never

##
# run on main branch
##

# let semantic-release check if release and add tag in git if so
semantic release version:
  stage: deploy
  image: python:3.11-bookworm
  variables:
    GITLAB_TOKEN: ${GL_TOKEN}
    GIT_COMMIT_AUTHOR: ${GL_USER} <${GL_USER}@noreply.gitlab.gwdg.de>
    GIT_STRATEGY: clone
  before_script:
    - git checkout "$CI_COMMIT_REF_NAME"
    - pip install python-semantic-release
  script:
    - semantic-release version
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

##
# run on tags
##

# if there is a tag, run the release upload to pypi.org and publish release to gitlab
publish release:
  stage: deploy
  image: python:3.11-bookworm
  variables:
    GITLAB_TOKEN: ${GL_TOKEN}
    GIT_COMMIT_AUTHOR: ${GL_USER} <${GL_USER}@noreply.gitlab.gwdg.de>
  before_script:
    - pip install python-semantic-release build
  script:
    - python -m build
    - semantic-release publish
  rules:
    - if: $CI_COMMIT_TAG

tag-release-image:
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  stage: deploy
  before_script:
    - *crane-setup
  script:
    - crane cp $CONTAINER_IMAGE $CONTAINER_RELEASE_IMAGE
    - crane tag $CONTAINER_RELEASE_IMAGE latest
  rules:
    - if: $CI_COMMIT_TAG

# upload SBOMs
.upload-container-sbom:
  stage: deploy
  variables:
    X_API_KEY: $DEPS_UPLOAD_TOKEN
    AUTO_CREATE_PROJECT: 'true'
    PROJECT_NAME: $CI_PROJECT_NAME-container
    BOM_LOCATION: 'sbom.container.json'
  extends:
    - .upload-bom-to-deps

upload-dev-container-sbom:
  extends: .upload-container-sbom
  stage: deploy
  variables:
    PROJECT_VERSION: $CI_COMMIT_TAG
  rules:
    - if: $CI_COMMIT_TAG

upload-prod-sbom:
  stage: deploy
  variables:
    X_API_KEY: $DEPS_UPLOAD_TOKEN
    AUTO_CREATE_PROJECT: 'true'
    PROJECT_NAME: $CI_PROJECT_NAME
    PROJECT_VERSION: $CI_COMMIT_TAG
    BOM_LOCATION: 'bom.json'
  extends:
    - .upload-bom-to-deps
  rules:
    - if: $CI_COMMIT_TAG
