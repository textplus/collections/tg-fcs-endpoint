# CHANGELOG



## v0.4.1 (2024-03-04)

### Fix

* fix: keep the v in tag name, but use commit title for docker image tag ([`2e60a8e`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/2e60a8e96425f638389a8da09145ab4ecc77d6fd))

* fix: we want no v in tag names, just the number ([`d096c27`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/d096c278d630d5781bcbd07bda084fc473005eb1))


## v0.4.0 (2024-03-04)

### Build

* build(pip): update requirements ([`1367121`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/1367121f7ab75a60b1763afe313694b3caade057))

### Chore

* chore: black ([`94345d9`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/94345d995cd9d2fb37098bfe19aedde464c18eee))

* chore: let semantic-release changelog exclude commit starting with &#39;Merge branch*&#39; ([`6c7fb59`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/6c7fb591df4742ea7ce965bceb392d08266fe266))

### Ci

* ci(gitlab): add some workflow debug output ([`4739925`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/4739925d9664d9ba696f381b020cf728c713b27d))

### Feature

* feat: maintain version with sematic-release and show on index page ([`63069fa`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/63069fa4bb6c84ac71f1307ca769cebeedde5df1))


## v0.3.0 (2024-02-29)

### Chore

* chore: license ([`cabce09`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/cabce090c7cd6bf6435dd489c05fd835eac53803))

* chore: missing template file for last commit ([`2a5477f`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/2a5477f5936fdea767057b1725dbbf266a229f09))

### Feature

* feat: project listing and hints on config for index page ([`56a8adc`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/56a8adc7eb92a4b290ce6b36544f621981555038))

### Unknown

* doc: text+ ([`96d29d5`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/96d29d5d7c847b34547b5fa16e5589ab1d1cd5d9))


## v0.2.0 (2024-02-29)

### Build

* build: relax python version requirement to 3.10 ([`ab380ed`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/ab380ed3860b5b33b885ce651749a49fcd8b438c))

### Chore

* chore: add license headers ([`41642dd`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/41642dd7d1d2469c8631f0a52d66d334ee664898))

### Documentation

* docs: typo ([`1d58232`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/1d58232221f7552b43d261cf93872b4ff33c394e))

* docs: add missing files for composing local aggregator ([`80bf985`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/80bf985f51b030747bb1d9fab309eec670b4664e))

* docs: add aggregator to docker-compose for local testing ([`e0b223c`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/e0b223c0bfba1362da31a7484c575f98a8012356))

### Feature

* feat: return handle as pid ([`c697ef8`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/c697ef8b192cbc73c4a83b82f1100db77efcaddd))

* feat: generate own endpoint description and pid for every project ([`9f5761a`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/9f5761ac8e3584974fda9954a408b18396ad3283))

### Unknown

* doc: some typos ([`837c391`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/837c3913210175ab062d1af8db7f99ded0a5bd85))

* doc: add textplus ui widget to docker-compose and document how to use ([`9adfcb9`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/9adfcb924e177e47882e02c7e89b0b115d08b370))


## v0.1.2 (2024-02-28)

### Fix

* fix(ci): change rule order ([`3fa57f3`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/3fa57f393af0bf440fa0acaa02c596a30f657386))


## v0.1.1 (2024-02-27)

### Fix

* fix(ci): release pipeline should only run on tags with numbers, and not trigger pipeline runs ([`d0a355d`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/d0a355de06f005d97b4a000cc9ac8fe28e05fff4))


## v0.1.0 (2024-02-27)

### Build

* build: dependency updates ([`d2cc82c`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/d2cc82cd330c9aa7859987aee43f5a6f1d5c64eb))

### Chore

* chore: config for semantic release ([`a262cb1`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/a262cb1862d3840326c8f89ca2bd41a808f38425))

* chore: black and format ([`190ce2f`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/190ce2fd6910280b3abd71d2b4cbe33c462fcfb3))

* chore: add .editorconfig ([`750b2f0`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/750b2f04f7271c5c612fffef2f433e24df103ec4))

### Ci

* ci(gitlab): cyclondx syntax changed ([`fc1f689`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/fc1f6899ca07515da19f208fdce9f3fa70259029))

### Feature

* feat: semantic release workflow ([`24880bc`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/24880bc6b6f5d4009aac21c43e0e8359cb17b863))

* feat: configure textgrid host from env, reuse tgsearch client ([`2664c73`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/2664c739885a703bebbe31a972074ad5f94eef0a))

* feat: add root resource for k8s liveness and readyness probes ([`4999e5e`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/4999e5eee9295ec18d4dcfaa77fbef86a2e536f6))

### Refactor

* refactor: rename env var, show env vars in index html ([`6463f20`](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint/-/commit/6463f20494c271bf2b1906146f3c0085adfceffd))
