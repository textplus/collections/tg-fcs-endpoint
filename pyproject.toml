# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

[project]
name = "tg-fcs-endpoint"
description = "FCS endpoint for the TextGrid repository"

requires-python = ">= 3.10"
dynamic = ["version"]

readme = "README.md"
license = { file = "LICENSE" }

authors = [
  {name = "Florian Barth", email = "barth@sub.uni-goettingen.de"},
  {name = "Ubbo Veentjer", email = "veentjer@sub.uni-goettingen.de"},
]

maintainers = [
  {name = "Florian Barth", email = "barth@sub.uni-goettingen.de"},
  {name = "Ubbo Veentjer", email = "veentjer@sub.uni-goettingen.de"},
]

dependencies = [
    "fcs-simple-endpoint",
    "tgclients",
    "Flask",
]

[project.optional-dependencies]
dev = [
    "reuse",
    "black",
    "pip-tools",
]

server = [
    "gunicorn"
]

[project.urls]
repository = "https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint"

[tool.setuptools.packages.find]
where = ["src"]

[tool.setuptools.dynamic]
version = {attr = "tg-fcs-endpoint.__version__"}

[tool.semantic_release]
major_on_zero = false
version_variables = ["src/tg-fcs-endpoint/__init__.py:__version__"]
# if you want a version without you need to set this option early
# or rename all the tags done before
#tag_format = "{version}"

[tool.semantic_release.remote]
type = "gitlab"

[tool.semantic_release.changelog]
exclude_commit_patterns = ["Merge branch*"]
