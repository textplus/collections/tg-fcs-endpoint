# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: MIT

from typing import Any, Dict, Optional, Union
from os import getenv, path
import logging

from flask import Flask, Response, request, render_template

import requests  # TODO: remove with tgclients update

import cql
from clarin.sru.server.request import SRURequest
from clarin.sru.server.result import SRUSearchResultSet, SRUExplainResult
from clarin.sru.fcs.constants import FCS_NS, FCSQueryType, X_FCS_ENDPOINT_DESCRIPTION
from clarin.sru.queryparser import CQLQuery, SRUQuery, SRUQueryParserRegistry
from clarin.sru.diagnostic import SRUDiagnosticList
from clarin.sru.fcs.xml.writer import FCSRecordXMLStreamWriter

from clarin.sru.exception import SRUException

from tgclients import TextgridSearch, TextgridConfig
from tgclients.config import PROD_SERVER
from tgclients.databinding.tgsearch import (
    Response as SearchResponse,
    ResultType,
    FulltextType,
)

from clarin.sru.constants import (
    SRUDiagnostics,
    SRUVersion,
)

from clarin.sru.server.config import (
    DatabaseInfo,
    IndexInfo,
    LocalizedString,
    SchemaInfo,
    SRUServerConfig,
    SRUServerConfigKey,
)

from clarin.sru.fcs.server.search import (
    DataView,
    EndpointDescription,
    ResourceInfo,
    SimpleEndpointDescription,
    SimpleEndpointSearchEngineBase,
)

from clarin.sru.server.server import SRUServer
from clarin.sru.xml.writer import SRUXMLStreamWriter

from . import __version__

MIMETYPE_TEI_XML = "application/x-tei+xml"

logger = logging.getLogger(__name__)

template_dir = path.abspath(path.dirname(__file__))
template_dir = path.join(template_dir, "templates")
app = Flask(__name__, template_folder=template_dir)

# config textgrid server urls from env
TEXTGRID_HOST = getenv("TEXTGRID_HOST", PROD_SERVER)
TGREP_PORTAL_HOST = getenv("TGREP_PORTAL_HOST", "https://textgridrep.org")
SERVICE_HOST = getenv("SERVICE_HOST", "https://fcs.textgridrep.org")

# only instantiate tgsearch once globally, for connection and databinding reuse
tgconf = TextgridConfig(TEXTGRID_HOST)
tgsearch = TextgridSearch(tgconf)


def _cql2cqp(query: CQLQuery) -> str:
    node: Union[cql.parser.CQLTriple, cql.parser.CQLSearchClause] = (
        query.parsed_query.root
    )

    if isinstance(node, cql.parser.CQLTriple):
        operator = node.operator.value
        raise SRUException(
            SRUDiagnostics.UNSUPPORTED_BOOLEAN_OPERATOR,
            operator,
            message=f"Unsupported Boolean operator: {operator}",
        )

    if isinstance(node, cql.parser.CQLSearchClause):
        terms = node.term.lower().split()
        if len(terms) == 1:
            return f"{terms[0]}"

        terms = [term.strip("\"'") for term in terms]
        return " ".join(f"{term}" for term in terms)

    raise SRUException(
        SRUDiagnostics.CANNOT_PROCESS_QUERY_REASON_UNKNOWN,
        f"unknown cql node: {node}",
    )


class TgFCSSearchResultSet(SRUSearchResultSet):

    def __init__(
        self,
        results: SearchResponse,
        diagnostics: SRUDiagnosticList,
        resource_pid: str,
        request: Optional[SRURequest] = None,
    ) -> None:
        super().__init__(diagnostics)
        self.request = request
        self.results = results
        self.resource_pid = resource_pid

        self.current_record_cursor = 0
        if request:
            self.maximum_records = request.get_maximum_records()
            self.record_count = request.get_maximum_records()
        else:
            self.maximum_records = 250
            self.record_count = 250

    def get_record_count(self) -> int:
        return len(self.results.result)

    def get_total_record_count(self) -> int:
        if self.results:
            return int(self.results.hits)
        return -1

    def next_record(self) -> bool:
        if self.current_record_cursor < min(
            len(self.results.result), self.maximum_records
        ):
            self.current_record_cursor += 1
            return True
        return False

    def get_record_identifier(self) -> str:
        return None

    def get_record_schema_identifier(self) -> str:
        if self.request:
            rsid = self.request.get_record_schema_identifier()
            if rsid:
                return rsid
        return FCS_NS  # CLARIN_FCS_RECORD_SCHEMA

    def write_record(self, writer: SRUXMLStreamWriter) -> None:
        result: ResultType = self.results.result[self.current_record_cursor - 1]

        tguri: str = result.object_value.generic.generated.textgrid_uri.value
        title: str = result.object_value.generic.provided.title[0]

        if (
            hasattr(result.object_value.generic.generated, "pid")
            and len(result.object_value.generic.generated.pid) > 0
        ):
            pid: str = result.object_value.generic.generated.pid[0].value
        else:
            pid: str = tguri

        FCSRecordXMLStreamWriter.startResource(
            writer, pid=pid, ref=f"{TGREP_PORTAL_HOST}/{tguri}"
        )
        FCSRecordXMLStreamWriter.startResourceFragment(
            writer, pid=pid
        )  # ref im fragment wäre gut um an die exakte fundstelle zu springen (wie?)

        if len(result.fulltext) > 0:
            kwic: FulltextType.Kwic = result.fulltext[0].kwic[0]
            FCSRecordXMLStreamWriter.writeSingleHitHitsDataView(
                writer, kwic.left, kwic.match, kwic.right
            )
        else:
            FCSRecordXMLStreamWriter.writeSingleHitHitsDataView(
                writer, "Titel:", title, ""
            )

        FCSRecordXMLStreamWriter.endResourceFragment(writer)
        FCSRecordXMLStreamWriter.endResource(writer)


class TgFCSEndpointSearchEngine(SimpleEndpointSearchEngineBase):

    def __init__(
        self,
        endpoint_description: EndpointDescription,
    ) -> None:
        super().__init__()
        self.endpoint_description = endpoint_description

    def create_EndpointDescription(
        self,
        config: SRUServerConfig,
        query_parser_registry_builder: SRUQueryParserRegistry.Builder,
        params: Dict[str, str],
    ) -> EndpointDescription:
        return self.endpoint_description

    def do_init(
        self,
        config: SRUServerConfig,
        query_parser_registry_builder: SRUQueryParserRegistry.Builder,
        params: Dict[str, str],
    ) -> None:
        pass

    def search(
        self,
        config: SRUServerConfig,
        request: SRURequest,
        diagnostics: SRUDiagnosticList,
    ) -> SRUSearchResultSet:
        query: str

        if request.is_query_type(FCSQueryType.CQL):
            # Got a CQL query (either SRU 1.1 or higher).
            # Translate to a plain query string ...
            query_in: SRUQuery = request.get_query()
            assert isinstance(query_in, CQLQuery)
            query = _cql2cqp(query_in)
        else:
            # Got something else we don't support. Send error ...
            raise SRUException(
                SRUDiagnostics.CANNOT_PROCESS_QUERY_REASON_UNKNOWN,
                f"Queries with queryType '{request.get_query_type()}' are not supported by this CLARIN-FCS Endpoint.",
            )

        logger.info(query)

        project_filter = None
        if hasattr(request.request, "tg_project_id"):
            # print("search within project: ", request.request.tg_project_id)
            project_filter = f"project.id:{request.request.tg_project_id}"

        results = tgsearch.search(
            query,
            limit=request.get_maximum_records(),
            start=request.get_start_record(),
            filters=project_filter,
            target="structure",
        )
        # print(f"query for '{query}' with filter: '{project_filter}', limit: '{request.get_maximum_records()}' and start: '{request.get_start_record()}'")

        return TgFCSSearchResultSet(
            results, diagnostics=diagnostics, resource_pid="TODO-PID"
        )

    def explain(
        self,
        config: SRUServerConfig,
        request: SRURequest,
        diagnostics: SRUDiagnosticList,
    ) -> Optional[SRUExplainResult]:
        val = request.get_extra_request_data(X_FCS_ENDPOINT_DESCRIPTION)
        provide_epdesc = SimpleEndpointSearchEngineBase._parse_bool(val)

        if hasattr(request.request, "tg_project_id"):
            project_id = request.request.tg_project_id
            description = self.build_tgproject_endpoint_description(project_id)
        else:
            description = self.endpoint_description

        if provide_epdesc:

            class SRUExplainResultWithEndpointDescription(SRUExplainResult):
                def __init__(
                    self,
                    diagnostics: SRUDiagnosticList,
                    endpoint_description: EndpointDescription,
                ) -> None:
                    super().__init__(diagnostics)
                    self.endpoint_description = endpoint_description

                @property
                def has_extra_response_data(self) -> bool:
                    return True

                def write_extra_response_data(self, writer: SRUXMLStreamWriter) -> None:
                    SimpleEndpointSearchEngineBase._write_EndpointDescription(
                        writer, self.endpoint_description
                    )

            return SRUExplainResultWithEndpointDescription(diagnostics, description)

        return None

    def build_tgproject_endpoint_description(self, project_id) -> EndpointDescription:

        project_info = get_tg_project(project_id)

        dataviews = [
            DataView(
                identifier="hits",
                mimetype="application/x-clarin-fcs-hits+xml",
                deliveryPolicy=DataView.DeliveryPolicy.SEND_BY_DEFAULT,
            ),
            DataView(
                identifier="tei",
                mimetype=MIMETYPE_TEI_XML,
                deliveryPolicy=DataView.DeliveryPolicy.SEND_BY_DEFAULT,
            ),
        ]
        resources = [
            ResourceInfo(
                pid=project_id,  # TODO eindeutiger identifier der resource (projekt-id z.b.)
                title={
                    "en": project_info["name"]
                },  # TODO title der resource (projekt name)
                description=(
                    project_info["description"]
                    if hasattr(project_info, "description")
                    else ""
                ),
                landing_page_uri=TGREP_PORTAL_HOST + "/project/" + project_id,
                languages=["deu", "eng"],  # TODO
                available_DataViews=dataviews,
            )
        ]
        return SimpleEndpointDescription(
            version=2,
            capabilities=["http://clarin.eu/fcs/capability/basic-search"],
            supported_DataViews=dataviews,
            supported_Layers=[],
            resources=resources,
            pid_case_sensitive=False,
        )


class TgFcs:

    def __init__(self):
        self.server = self.build_fcs_server()

    def build_fcs_server_params(self) -> Dict[str, Any]:
        return {
            SRUServerConfigKey.SRU_SUPPORTED_VERSION_MIN: SRUVersion.VERSION_1_1.version_string,
            SRUServerConfigKey.SRU_SUPPORTED_VERSION_MAX: SRUVersion.VERSION_2_0.version_string,
            SRUServerConfigKey.SRU_SUPPORTED_VERSION_DEFAULT: SRUVersion.VERSION_2_0.version_string,
            SRUServerConfigKey.SRU_TRANSPORT: "http",
            # SRUServerConfigKey.SRU_HOST: instance.config["api"]["host"],
            # SRUServerConfigKey.SRU_PORT: instance.config["api"]["port"],
            SRUServerConfigKey.SRU_HOST: "localhost",  # TODO
            SRUServerConfigKey.SRU_PORT: "5000",  # TODO
            # SRUServerConfigKey.SRU_DATABASE: self.path,
            SRUServerConfigKey.SRU_DATABASE: "/fcs",  # TODO
            SRUServerConfigKey.SRU_ECHO_REQUESTS: "true",
            SRUServerConfigKey.SRU_ALLOW_OVERRIDE_MAXIMUM_RECORDS: "true",
            SRUServerConfigKey.SRU_ALLOW_OVERRIDE_MAXIMUM_TERMS: "false",
            SRUServerConfigKey.SRU_ALLOW_OVERRIDE_INDENT_RESPONSE: "true",
        }

    def build_fcs_server_config(self, params: Dict[str, Any]) -> SRUServerConfig:
        database_info = DatabaseInfo(
            title=[
                LocalizedString(value="TextGrid Repository", lang="en", primary=True)
            ],
            author=[
                LocalizedString(
                    value=author,
                    lang="en",
                    primary=True if i == 0 else False,
                )
                for i, author in enumerate(["TODO"])
            ],
        )
        index_info = IndexInfo(
            sets=[
                IndexInfo.Set(
                    identifier="http://clarin.eu/fcs/resource",
                    name="fcs",
                    title=[
                        LocalizedString(
                            value="CLARIN Content Search",
                            lang="en",
                            primary=True,
                        )
                    ],
                )
            ],
            indexes=[
                IndexInfo.Index(
                    can_search=True,
                    can_scan=False,
                    can_sort=False,
                    maps=[IndexInfo.Index.Map(primary=True, set="fcs", name="words")],
                    title=[LocalizedString(value="Words", lang="en", primary=True)],
                )
            ],
        )
        schema_info = [
            SchemaInfo(
                identifier="http://clarin.eu/fcs/resource",
                name="fcs",
                location=None,
                sort=False,
                retrieve=True,
                title=[
                    LocalizedString(
                        value="CLARIN Content Search",
                        lang="en",
                        primary=True,
                    )
                ],
            )
        ]

        return SRUServerConfig.fromparams(
            params,
            database_info=database_info,
            index_info=index_info,
            schema_info=schema_info,
        )

    # TODO: this may be removed, as we generate by project now, no?
    def build_fcs_endpointdescription(self) -> EndpointDescription:
        dataviews = [
            DataView(
                identifier="hits",
                mimetype="application/x-clarin-fcs-hits+xml",
                deliveryPolicy=DataView.DeliveryPolicy.SEND_BY_DEFAULT,
            ),
            DataView(
                identifier="tei",
                mimetype=MIMETYPE_TEI_XML,
                deliveryPolicy=DataView.DeliveryPolicy.SEND_BY_DEFAULT,
            ),
        ]
        resources = [
            ResourceInfo(
                pid="TODO_PID",  # TODO eindeutiger identifier der resource (projekt-id z.b.)
                title={
                    "en": "TextGrid Repository"
                },  # TODO title der resource (projekt name)
                description=None,
                landing_page_uri=None,
                languages=["deu", "eng"],  # TODO
                available_DataViews=dataviews,
            )
        ]
        return SimpleEndpointDescription(
            version=2,
            capabilities=["http://clarin.eu/fcs/capability/basic-search"],
            supported_DataViews=dataviews,
            supported_Layers=[],
            resources=resources,
            pid_case_sensitive=False,
        )

    def build_fcs_server(self) -> SRUServer:
        params = self.build_fcs_server_params()
        config = self.build_fcs_server_config(params)
        qpr_builder = SRUQueryParserRegistry.Builder(True)
        search_engine = TgFCSEndpointSearchEngine(
            endpoint_description=self.build_fcs_endpointdescription(),
        )
        search_engine.init(config, qpr_builder, params)
        return SRUServer(config, qpr_builder.build(), search_engine)


tgfcs = TgFcs()


# TODO: this method will move to tgclients
def get_tg_project(project_id):
    url = TEXTGRID_HOST + "/1.0/tgsearch-public/portal/project/"
    response = requests.get(url + project_id, headers={"Accept": "application/json"})
    return response.json()


# TODO: this method will move to tgclients
def get_tg_projects():
    url = TEXTGRID_HOST + "/1.0/tgsearch-public/portal/projects"
    response = requests.get(url, headers={"Accept": "application/json"})
    return response.json()


@app.route("/")
def handle_root():
    return render_template(
        "index.html",
        tg_host=TEXTGRID_HOST,
        tgrep_host=TGREP_PORTAL_HOST,
        service_host=SERVICE_HOST,
        projects=get_tg_projects()["projects"],
        version=__version__,
    )


@app.route("/<project_id>/fcs")
def handle_project(project_id: str) -> Response:
    # attaching a new param to the request class to pass to search method works in python
    # obviously bad style, no?
    request.tg_project_id = project_id
    response = Response()
    tgfcs.server.handle_request(request, response)
    return response


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
