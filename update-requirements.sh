#!/bin/bash

# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

if ! command -v pip-compile &> /dev/null || ! command -v reuse &> /dev/null
then
    echo "command 'pip-compile' and/or 'reuse' could not be found."
    echo "make sure to run this script from python venv, check the README.md"
    exit 1
fi

pip-compile --extra server --generate-hashes --upgrade pyproject.toml
reuse annotate --copyright="Georg-August-Universität Göttingen" --license CC0-1.0 requirements.txt
